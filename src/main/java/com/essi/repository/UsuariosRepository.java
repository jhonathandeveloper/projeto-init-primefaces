package com.essi.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import com.essi.model.Usuarios;

public class UsuariosRepository implements Serializable{

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager manager;
	
	
	public List<Usuarios> buscarTodos(){
		return manager.createQuery("from Usuarios", Usuarios.class).getResultList();
	}


	public Usuarios porId(Long id) {
		return manager.find(Usuarios.class, id);
	}


	public Usuarios salvar(Usuarios usuarioModel) {
		EntityTransaction trx = manager.getTransaction();
		trx.begin();
		
		usuarioModel = manager.merge(usuarioModel);
		
		trx.commit();
		
		return usuarioModel;
	}


	public boolean valida(Usuarios usuarioModel) {
		return true;
	}
}
