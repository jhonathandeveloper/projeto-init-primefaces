package com.essi.service;

import javax.inject.Inject;

import com.essi.model.Usuarios;
import com.essi.repository.UsuariosRepository;

public class UsuarioService {

	@Inject
	private UsuariosRepository usuariosRepository;
	
	public Usuarios salvar(Usuarios usuarioModel) {
		boolean valida = usuariosRepository.valida(usuarioModel);
		if(!valida) {
			throw new NegocioException("Validação com Erro!");
		}
		return usuariosRepository.salvar(usuarioModel);
	}

}
