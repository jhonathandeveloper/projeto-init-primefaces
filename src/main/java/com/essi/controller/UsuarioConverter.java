package com.essi.controller;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.essi.model.Usuarios;
import com.essi.repository.UsuariosRepository;
import com.essi.util.cdi.CDIServiceLocator;

@FacesConverter(forClass = Usuarios.class)
public class UsuarioConverter implements Converter {

	private UsuariosRepository usuariosRepository;
	
	public UsuarioConverter() {
		usuariosRepository = CDIServiceLocator.getBean(UsuariosRepository.class);
	}
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		Usuarios retorno = null;
		
		if(value != null) {
			Long id = new Long(value);
			retorno = usuariosRepository.porId(id);
		}
		return retorno;
	}
	
	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if(value != null) {
			return ((Usuarios) value).getId().toString();
		}
		return "";
	}
}
 