package com.essi.controller;

import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.essi.model.Usuarios;
import com.essi.repository.UsuariosRepository;
import com.essi.service.UsuarioService;
import com.essi.util.jsf.FacesUtil;

@Named
@ViewScoped
public class HomeController implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private UsuariosRepository usuariosRepository;
	
	//Implementa Validação como para salvar chamar ele primeiro e dentro dele chama
	//Repository
	@Inject
	private UsuarioService usuarioService;

	private List<Usuarios> usuarios;
	
	private Usuarios usuarioModel;

	public void inicializar() {
		System.out.println("Inicializando");
		if (FacesUtil.isNotPostback()) {
			usuarios = usuariosRepository.buscarTodos();
			for (Usuarios usuarios2 : usuarios) {
				System.out.println(usuarios2);
			}
		}
	}

	public void salvar() {
		usuarioModel = usuarioService.salvar(this.usuarioModel);
		
		FacesUtil.addInfoMessage("Usuário Salvo com Sucesso!");
	}
	
	public List<Usuarios> getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(List<Usuarios> usuarios) {
		this.usuarios = usuarios;
	}
}
